//
//  ViewController.swift
//  Jcontato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome: String
    let numero: String
    let email: String
    let endereco: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContato:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContato.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let contato = listaDeContato[indexPath.row]
        
        cell.nome.text = contato.nome
        cell.numero.text = contato.numero
        cell.email.text = contato.email
        cell.endereco.text = contato.endereco
        
        return cell
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        
        listaDeContato.append(Contato(nome: "Amir", numero: "00000001", email: "1@gmail.com", endereco: "Rua 1"))
        listaDeContato.append(Contato(nome: "Carlos", numero: "00000002", email: "2@gmail.com", endereco: "Rua 2"))
        listaDeContato.append(Contato(nome: "Vladmir", numero: "00000003", email: "3@gmail.com", endereco: "Rua 3"))
        listaDeContato.append(Contato(nome: "Heimar", numero: "00000004", email: "4@gmail.com", endereco: "Rua 4"))
        
    }


}


